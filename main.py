import sys
def find_min_bridges_anticlockwise(strands,bridge_number,fav_strand,bridges):
    # To find the bridge distances using dict
    bridge_anticlockwise = [0 for i in range(strands)]
    bridges_dict = {}
    for distance,start in bridges:
        if distance in bridges_dict:
            bridges_dict[distance].append(start)
        else:
            bridges_dict[distance] = [start]
    print('Bridges : ',bridges_dict)
    #finding distances
    distances = sorted(bridges_dict.keys())
    print('Distances :',distances)

    # finding minimum no of bridges from start to fav_strand
    start_strand = fav_strand
    start_distance = distances[0]
    for i in range(1, len(distances)):  
        next_distance = distances[i]
        print(next_distance)
        if start_strand in bridges_dict[next_distance]:
            if len(bridges_dict[next_distance]) > 1:
                start_strand = bridges_dict[next_distance][(bridges_dict[next_distance].index(start_strand) + 1) % 2]
            else:
                start_strand = bridges_dict[next_distance][0]
            start_distance = next_distance
            print(start_distance,start_strand)
        else:
            bridge_anticlockwise[start_strand] += (next_distance - start_distance - 1)
            print("bridges_anticlockwise : ",bridge_anticlockwise)
            #bridges_anticlockwise = False
    operation = [1 if i==0 else 0 for i in bridge_anticlockwise]
    return operation


#creates a list of bridges using the input values
strands = int(sys.argv[1])  
bridges_number = int(sys.argv[2])
fav_place = int(sys.argv[3]) 
bridges = []
for i in range(strands):
	x, y = map(int, sys.argv[4+2*i:4+2*(i+1)])
	bridges.append((x, y))

print(strands, bridges_number, fav_place)
print(bridges)
print(find_min_bridges_anticlockwise(strands,bridges_number,fav_place,bridges))